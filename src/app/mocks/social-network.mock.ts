import { SocialNetwork } from "src/app/landing-page/navbar-social/social-network";

export const SocialNetworkMock: SocialNetwork[] = [
  {
    class: 'fa-facebook-f',
    name: 'Facebook',
    status: true,
    url: 'https://fb.com',
  },
  {
    class: 'fa-twitter',
    name: 'Twitter',
    status: true,
    url: 'https://twitter.com',
  },
  {
    class: 'fa-youtube',
    name: 'Youtube',
    status: true,
    url: 'https://youtube.com',
  },
  {
    class: 'fa-instagram',
    name: 'Instagram',
    status: true,
    url: 'https://instagram.com',
  },
];
