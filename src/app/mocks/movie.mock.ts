import { Movie } from "src/app/models/movie.models";

export const MovieMock:Movie[] = [
  {
    billboard: true,
    description:
      'Superman (Henry Cavill) se ha convertido en la figura más controvertida del mundo. Mientras que muchos siguen creyendo que es un emblema de esperanza, otro gran número de personas lo consideran una amenaza para la humanidad. Para el influyente Bruce Wayne (Ben Affleck), Superman es claramente un peligro para la sociedad, su poder resulta imprudente y alejado de la mano del gobierno. Por eso, ante el temor de las acciones que pueda llevar a cabo un superhéroe con unos poderes casi divinos, decide ponerse la máscara y la capa para poner a raya al superhéroe de Metrópolis. Mientras que la opinión pública debate sobre el interrogante de cuál es realmente el héroe que necesitan, el Hombre de Acero y Batman, enfrentados entre sí, se sumergen en una contienda el uno contra el otro. La rivalidad entre ellos está alimentada por el rencor y la venganza, y nada puede disuadirlos de librar esta guerra. Hostigados por el multimillonario Lex Luthor (Jesse Eisenberg), Batman y Superman se ven las caras en una lucha sin precedentes. Pero las cosas se complican cuando una nueva y peligrosa amenaza pronto cobra fuerza, poniendo a toda la humanidad en el mayor peligro que nunca se haya conocido antes. Esta nueva y oscura amenaza, que surge con la figura de un tercer hombre con poderes superlativos llamado Doomsday, puede poner en serio peligro al mundo y causar la destrucción total. Será entonces cuando el Último Hijo de Krypton y el Caballero Oscuro unan sus fuerzas con Wonder Woman (Gal Gadot) para enfrentarse todos juntos a esta amenazante nueva máquina de matar.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/BatmanVSSuperman.jpg?alt=media&token=cedab083-7b3f-441a-acb0-f00b5221f385',
    likes: 54,
    name: 'Batman vs Superman',
    trailer: 'https://www.youtube.com/watch?v=0WWzgGyAH6Y',
  },
  {
    billboard: true,
    description:
      'En un futuro no muy lejano, legiones de gigantescas criaturas monstruosas comienzan a salir misteriosamente de las profundidades del mar con el objetivo de iniciar una guerra contra la humanidad que puede destruir millones de vidas y agotar todos los recursos que el ser humano necesita para sobrevivir. Estas terribles criaturas, hasta entonces desconocidas por la raza humana, son denominadas Kaiju, nombre que hace referencia a las películas sobre grandes y espeluznantes criaturas propias del género cinematográfico japonés que comparte el mismo nombre.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/pacific-rim.jpg?alt=media&token=a82b8906-b87d-4c3c-be03-f1d2bee6d096',
    likes: 36,
    name: 'Pacific Rim 2013',
    trailer: 'https://www.youtube.com/watch?v=K-ZcqwvQbas',
  },
  {
    billboard: true,
    description:
      'Arizona, 1873. Un extraño (Craig), que no recuerda su pasado, acaba por casualidad en el duro y desértico pueblo de Absolution. La única pista sobre esta misteriosa historia es un grillete que lleva enganchado en su muñeca. Pronto descubre que los forasteros no son bienvenidos en Absolution y que nadie mueve un dedo en sus calles sin que se lo ordene el Coronel (mano de hierro) Dolarhyde Ford. Es un pueblo que vive sumido en el miedo. Pero Absolution está a punto de experimentar un pánico incompresible cuando la desolada ciudad es atacada por unos maleantes desde el cielo. Haciendo un gran ruido y a una impresionante velocidad, las luces cegadoras abducen a los indefensos uno a uno, desafiando todo lo que los residentes conocían hasta ahora.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/cowboys_aliens.jpg?alt=media&token=c6cfe4c7-7c63-49cc-b225-0b846c2c4b85',
    likes: 61,
    name: 'Cowboys & Aliens',
    trailer: 'https://www.youtube.com/watch?v=MqCp2PmRyXg',
  },
  {
    billboard: true,
    description:
      'Una historia alrededor de la población vasca bombardeada por la aviación nazi en abril de 1937, durante la Guerra Civil Española. En ese contexto, la joven Teresa (María Valverde), una editora de la oficina de prensa republicana chocará con Henry (James D’Arcy), un periodista americano en horas bajas que está cubriendo el conflicto. Teresa, cortejada por su jefe, Vasyl (Jack Davenport), asesor soviético del gobierno republicano, se sentirá atraída por el idealismo durmiente de Henry y querrá despertar en él la pasión por contar la verdad, que un día fue su único objetivo.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/gernika.jpg?alt=media&token=bba459dc-2a86-4324-9421-a787b2caccd3',
    likes: 12,
    name: 'Gernika',
    trailer: 'https://www.youtube.com/watch?v=uuKl2dIiUzM',
  },
  {
    billboard: true,
    description:
      'El temerario aventurero Peter Quill es objeto de un implacable cazarrecompensas después de robar una misteriosa esfera codiciada por Ronan, un poderoso villano cuya ambición amenaza todo el universo. Para poder escapar del incansable Ronan, Quill se ve obligado a pactar una complicada tregua con un cuarteto de disparatados inadaptados: Rocket, un mapache armado con un rifle, Groot, un humanoide con forma de árbol, la letal y enigmática Gamora y el vengativo Drax the Destroyer. Pero cuando Quill descubre el verdadero poder de la esfera, deberá hacer todo lo posible para derrotar a sus extravagantes rivales en un intento desesperado de salvar el destino de la galaxia.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/guardians-galaxy.jpg?alt=media&token=4f132cfc-ff97-4154-9077-8a9c00755e14',
    likes: 98,
    name: 'Guardianes de la Galaxia',
    trailer: 'https://www.youtube.com/watch?v=qdIuXCfUKM8',
  },
  {
    billboard: true,
    description:
      'Cuando un meteorito lleno de mugre espacial transforma a Susan Murphy en una gigante, el Gobierno le concede el nombre de Ginormica y la encierra en una instalación secreta con otros monstruos, como el Dr. Cucaracha, cabeza de insecto. Cuando un robot extraterrestre llega a la Tierra y empieza una masacre, el general W.R. Monger convence al presidente para que envíe a Ginormica y a los monstruos a enfrentarse a la máquina y salvar al planeta.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/mostruos_vs_alienigenas.jpg?alt=media&token=af2442e1-6415-4ef0-9a84-8e6720981c67',
    likes: 33,
    name: 'Monstruos vs Alienígenas',
    trailer: 'https://www.youtube.com/watch?v=UzZRJTeMb0E',
  },
  {
    billboard: false,
    description:
      'El Imperio Galáctico ha terminado de construir el arma más poderosa de todas, la Estrella de la muerte, pero un grupo de rebeldes decide realizar una misión de muy alto riesgo: robar los planos de dicha estación antes de que entre en operaciones, mientras se enfrentan también al poderoso Lord Sith conocido como Darth Vader, discípulo del despiadado Emperador Palpatine. Film ambientado entre los episodios III y IV de Star Wars.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/RogueOneP.jpg?alt=media&token=cdf6c29a-ba4c-443f-b5ef-0359878217d8',
    likes: 0,
    name: 'Rogue One',
    trailer: 'https://www.youtube.com/watch?v=frdj1zb9sMY',
  },
  {
    billboard: false,
    description:
      'Dos adolescentes pacientes de cáncer inician un viaje para reafirmar sus vidas y visitar a un escritor solitario en Ámsterdam.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/stars.jpg?alt=media&token=cb63cb0e-a191-4ba2-a9f0-e8c3cf6e4c5a',
    likes: 0,
    name: 'Bajo la misma Estrella',
    trailer: 'https://www.youtube.com/watch?v=9Lt8QAZkc94',
  },
  {
    billboard: false,
    description:
      'Ahmanet, una antigua princesa egipcia maldita, despierta en su cripta y, furiosa y malvada, siembra el terror entre los humanos. La única persona que puede detenerla y evitar que arrase Londres es un intrépido mercenario.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/the_mummy.jpg?alt=media&token=d59716da-ccab-4c70-a32d-ee56a52a0906',
    likes: 0,
    name: 'La Momia',
    trailer: 'https://www.youtube.com/watch?v=b6iqUM7bxk4',
  },
  {
    billboard: false,
    description:
      'El periodista Eddie Brock está investigando a Carlton Drake, el célebre fundador de Life Foundation. Brock establece una simbiosis con un ente alienígena que le ofrece superpoderes, pero el ser se apodera de su personalidad y le vuelve perverso.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/venom.jpg?alt=media&token=00a77cac-62ca-4c94-ae53-e462a8ff0ed5',
    likes: 0,
    name: 'Venom',
    trailer: 'https://www.youtube.com/watch?v=mYTmQWZkw10',
  },
  {
    billboard: false,
    description:
      'Un desertor de la escuela de medicina intenta ocultar su atracción hacia su nueva amiga (Zoe Kazan), una brillante artista que ya tiene novio.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/what_if.jpg?alt=media&token=7b7887d3-c4e3-47d4-aa32-2b56d34e8901',
    likes: 0,
    name: '¿Sólo amigos?',
    trailer: 'https://www.youtube.com/watch?v=-CuAGFDpVpU',
  },
  {
    billboard: false,
    description:
      'Diana, hija de dioses y princesa de las amazonas, nunca ha salido de su isla. Un día, en el contexto de la Primera Guerra Mundial, un piloto americano se estrella en su isla y Diana salva su vida; el piloto le explica que se está desarrollando una gran guerra que puede devastar el mundo, y Diana parte a la batalla.',
    language: 'Español',
    picture:
      'https://firebasestorage.googleapis.com/v0/b/cinemzx.appspot.com/o/wonder_woman.jpg?alt=media&token=2559f2f0-0250-44d9-9c23-4558a9f671e9',
    likes: 0,
    name: 'La mujer maravilla',
    trailer: 'https://www.youtube.com/watch?v=DYhqqzo2xOI',
  },
];
