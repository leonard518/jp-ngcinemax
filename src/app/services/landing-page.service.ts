import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SocialNetwork } from 'src/app/landing-page/navbar-social/social-network';
import { Movie } from 'src/app/models/movie.models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LandingPageService {
  apiNetworks: string = 'SocialNetwork.json';
  apiMovies: string = 'Movies.json';

  formats: string[] = [
    'Doblada 2D',
    'Doblada 3D',
    'Idioma Original 2D',
    'Idioma Original 3D',
  ];

  hours: string[] = [...Array(5).keys()].map((i) => {
    return `${i + i + 2}:00pm`;
  });

  constructor(private http: HttpClient) {}

  getSocialNetworks(): Observable<SocialNetwork[]> {
    return this.http.get<SocialNetwork[]>(
      `${environment.API}/${this.apiNetworks}`
    );
  }

  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${environment.API}/${this.apiMovies}`);
  }
}
