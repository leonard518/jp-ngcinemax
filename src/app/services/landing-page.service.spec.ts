import { getTestBed, TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { LandingPageService } from './landing-page.service';
import { SocialNetwork } from 'src/app/landing-page/navbar-social/social-network';
import { SocialNetworkMock } from 'src/app/mocks/social-network.mock';
import { Movie } from 'src/app/models/movie.models';
import { MovieMock } from 'src/app/mocks/movie.mock';

describe('LandingPageService', () => {
  let service: LandingPageService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let mockNetworksResponse: SocialNetwork[] = SocialNetworkMock;
  let mockMoviesResponse: Movie[] = MovieMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    injector = getTestBed();
    httpMock = injector.inject(HttpTestingController);
    service = TestBed.inject(LandingPageService);
  });

  it('Debe crear el servicio', () => {
    expect(service).toBeTruthy();
  });

  it('Debe llamar el metodo getSocialNetworks() y retornar las redes sociales', () => {
    service.getSocialNetworks().subscribe((networks) => {
      expect(networks.length).toBe(4);
      expect(networks).toEqual(mockNetworksResponse);
    });
    const req = httpMock.expectOne(
      'https://cinemzx-default-rtdb.firebaseio.com/SocialNetwork.json'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockNetworksResponse);
  });

  it('Debe llamar el metodo getMovies() y retornar las movies', () => {
    service.getMovies().subscribe((movies) => {
      let billboards = movies.filter((movie) => !movie.billboard);
      let premieres = movies.filter((movie) => movie.billboard);

      // Validamos que al menos sean 6
      expect(billboards.length).toBeGreaterThanOrEqual(6);
      expect(premieres.length).toBeGreaterThanOrEqual(6);
    });
    const req = httpMock.expectOne(
      'https://cinemzx-default-rtdb.firebaseio.com/Movies.json'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockMoviesResponse);
  });

  it('La variable formatos debe contener 4 opciones', () => {
    let formats: string[] = service.formats;
    expect(formats.length).toEqual(4);
  });

  it('La variable horas debe contener 5 opciones', () => {
    let hours: string[] = service.hours;

    let myHours: string[] = [...Array(5).keys()].map((i) => {
      return `${i + i + 2}:00pm`;
    });
    console.log(myHours);

    expect(hours.length).toEqual(5);

    service.hours.map((i) => {
      expect(service.hours[i]).toEqual(myHours[i]);
    })

  });
});
