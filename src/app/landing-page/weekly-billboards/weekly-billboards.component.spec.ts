import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { MovieMock } from 'src/app/mocks/movie.mock';
import { Movie } from 'src/app/models/movie.models';
import { LandingPageService } from 'src/app/services/landing-page.service';
import { WeeklyBillboardsComponent } from './weekly-billboards.component';

describe('WeeklyBillboardsComponent', () => {
  let component: WeeklyBillboardsComponent;
  let fixture: ComponentFixture<WeeklyBillboardsComponent>;
  let service: LandingPageService;
  let mockMoviesResponse: Movie[] = MovieMock;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WeeklyBillboardsComponent],
      imports: [HttpClientTestingModule],
      providers: [LandingPageService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyBillboardsComponent);
    component = fixture.componentInstance;
    service = component.landingService;
    fixture.detectChanges();
  });

  it('Debe crear el componente', () => {
    expect(component).toBeTruthy();
  });

  it('Debe inyectar el servicio', () => {
    expect(component).toBeTruthy();
  });

  it('Debe llamar getMovies() de landingPageService', () => {
    let getMovies = spyOn(service, 'getMovies').and.returnValue(
      of(mockMoviesResponse)
    );
    let fixtureComponent = TestBed.createComponent(WeeklyBillboardsComponent);
    let compiled = fixtureComponent.debugElement.nativeElement;

    fixtureComponent.detectChanges();

    component.ngOnInit();
    expect(getMovies).toHaveBeenCalled();
    expect(compiled.querySelectorAll('.weekly-billboard').length).toBe(6);
    component.movies.map((movie) => {
      expect(movie.billboard).toBeTruthy();
    });
  });
});
