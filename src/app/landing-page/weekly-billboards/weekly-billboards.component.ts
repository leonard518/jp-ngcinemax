import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie.models';
import { LandingPageService } from 'src/app/services/landing-page.service';

@Component({
  selector: 'app-weekly-billboards',
  templateUrl: './weekly-billboards.component.html',
  styleUrls: ['./weekly-billboards.component.scss']
})
export class WeeklyBillboardsComponent implements OnInit {

  movies: Movie[] = [];

  constructor(public landingService: LandingPageService) { }

  ngOnInit(): void {
    this.getWeeklyBillboards();
  }

  getWeeklyBillboards(){
    this.landingService.getMovies().subscribe((movies) => {
      this.movies = movies.filter(movie => movie.billboard);
    })
  }
}
