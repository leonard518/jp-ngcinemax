import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LandingPageService } from 'src/app/services/landing-page.service';
import { Movie } from 'src/app/models/movie.models';
import { MovieMock } from 'src/app/mocks/movie.mock';
import { MoviesFilterComponent } from './movies-filter.component';
import { of } from 'rxjs';

declare var $: any;

describe('MoviesFilterComponent', () => {
  let component: MoviesFilterComponent;
  let fixture: ComponentFixture<MoviesFilterComponent>;
  let mockMoviesResponse: Movie[] = MovieMock;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MoviesFilterComponent],
      imports: [FormsModule, HttpClientTestingModule],
      providers: [LandingPageService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe generar los 8 items de fecha requeridos', () => {
    component.ngOnInit();

    [...Array(7).keys()].map((i) => {
      let date: Date = new Date();
      date.setDate(date.getDate() + i);
      expect(component.dates[i].toDateString()).toEqual(date.toDateString());
    });
  });

  it('Debe llamar getMovies() de landingPageService', () => {
    let service = fixture.debugElement.injector.get(LandingPageService);
    let spyGetMovies = spyOn(service, 'getMovies').and.callFake(() => {
      return of(mockMoviesResponse);
    });
    component.ngOnInit();

    expect(spyGetMovies).toHaveBeenCalled();

    component.movies.map((movie) => {
      expect(movie.billboard).toBeTruthy();
    });
  });
});
