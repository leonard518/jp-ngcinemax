import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie.models';
import { LandingPageService } from 'src/app/services/landing-page.service';
declare var $: any;

@Component({
  selector: 'app-movies-filter',
  templateUrl: './movies-filter.component.html',
  styleUrls: ['./movies-filter.component.scss'],
})
export class MoviesFilterComponent implements OnInit {
  dates: Date[] = [];
  movies: Movie[] = [];
  formats: string[] = this.landingPageService.formats;
  hours: string[] = this.landingPageService.hours;

  formFilter = {
    date: new Date(),
    movie: '',
    format: '',
    hour: '',
  };

  constructor(private landingPageService: LandingPageService) {}

  ngOnInit(): void {
    $('select').formSelect();
    this.createDates();
    this.getMovies();
  }

  createDates() {
    this.dates = [...Array(7).keys()].map((i) => {
      let date: Date = new Date();
      date.setDate(date.getDate() + i);
      return date;
    });
  }

  getMovies() {
    this.landingPageService
      .getMovies()
      .subscribe(
        (movies) => (this.movies = movies.filter((movie) => movie.billboard))
      );
  }
}
