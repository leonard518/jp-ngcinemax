import { Component, OnInit } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-carrousel',
  templateUrl: './carrousel.component.html',
  styleUrls: ['./carrousel.component.scss']
})
export class CarrouselComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $('.carousel').carousel();
  }

}
