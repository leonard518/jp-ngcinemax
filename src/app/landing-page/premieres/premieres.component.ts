import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie.models';
import { LandingPageService } from 'src/app/services/landing-page.service';

@Component({
  selector: 'app-premieres',
  templateUrl: './premieres.component.html',
  styleUrls: ['./premieres.component.scss']
})
export class PremieresComponent implements OnInit {

  movies: Movie[] = [];

  constructor(public landingService: LandingPageService) { }

  ngOnInit(): void {
    this.getPremieres();
  }

  getPremieres(){
    this.landingService.getMovies().subscribe((movies) => {
      this.movies = movies.filter(movie => !movie.billboard);
    })
  }

}
