import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { SocialNetwork } from 'src/app/landing-page/navbar-social/social-network';
import { SocialNetworkMock } from 'src/app/mocks/social-network.mock';
import { LandingPageService } from 'src/app/services/landing-page.service';

import { NavbarSocialComponent } from './navbar-social.component';

describe('NavbarSocialComponent', () => {
  let component: NavbarSocialComponent;
  let fixture: ComponentFixture<NavbarSocialComponent>;
  let service: LandingPageService;
  let mockNetworksResponse: SocialNetwork[] = SocialNetworkMock;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavbarSocialComponent],
      providers: [LandingPageService],
      imports: [HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarSocialComponent);
    component = fixture.componentInstance;
    service = component.landingService;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe inyectar LandingPageService', () => {
    expect(service).toBeTruthy();
  });

  it('Debe llenar la variable socialNetworks al llamar getSocialNetworks() de landingPageService', () => {
    spyOn(service, 'getSocialNetworks').and.returnValue(
      of(mockNetworksResponse)
    );
    component.ngOnInit();
    expect(component.socialNetworks).toEqual(mockNetworksResponse);
  });

  it('Debe llamar getSocialNetworks() en el ngOnInit()', () => {
    spyOn(component, 'getSocialNetworks').and.callThrough();

    component.ngOnInit();
    expect(component.getSocialNetworks).toHaveBeenCalled();
  });
});
