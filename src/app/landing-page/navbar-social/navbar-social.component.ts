import { Component, OnInit } from '@angular/core';
import { SocialNetwork } from 'src/app/landing-page/navbar-social/social-network';
import { LandingPageService } from 'src/app/services/landing-page.service';

@Component({
  selector: 'app-navbar-social',
  templateUrl: './navbar-social.component.html',
  styleUrls: ['./navbar-social.component.scss'],
})
export class NavbarSocialComponent implements OnInit {
  socialNetworks: SocialNetwork[] = [];

  constructor(public landingService: LandingPageService) {}

  ngOnInit(): void {
    this.getSocialNetworks();
  }

  getSocialNetworks(){
    this.landingService.getSocialNetworks().subscribe((networks) => {
      this.socialNetworks = networks;
    });
  }
}
