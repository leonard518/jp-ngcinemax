export interface SocialNetwork {
  class: string,
  name: string,
  status: boolean,
  url: string
}
