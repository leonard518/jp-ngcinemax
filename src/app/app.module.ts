import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NavbarSocialComponent } from 'src/app/landing-page/navbar-social/navbar-social.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { NavbarMenuComponent } from './landing-page/navbar-menu/navbar-menu.component';
import { MoviesFilterComponent } from './landing-page/movies-filter/movies-filter.component';
import { SlidesComponent } from './landing-page/slides/slides.component';
import { WeeklyBillboardsComponent } from './landing-page/weekly-billboards/weekly-billboards.component';
import { PremieresComponent } from './landing-page/premieres/premieres.component';
import { CarrouselComponent } from './landing-page/carrousel/carrousel.component';
import { FooterComponent } from './landing-page/footer/footer.component';
import { LandingPageService } from 'src/app/services/landing-page.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    NavbarSocialComponent,
    NavbarMenuComponent,
    MoviesFilterComponent,
    SlidesComponent,
    WeeklyBillboardsComponent,
    PremieresComponent,
    CarrouselComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [LandingPageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
