export interface Movie {
  billboard: boolean;
  description: string;
  language: string;
  likes: number;
  name: string;
  picture: string;
  trailer: string;
}
